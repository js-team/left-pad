## left-pad

String left pad

This is a meant to be a simple wrapper package for left-pad. 

left-pad is now deprecated, and using String.prototype.padStart() is recommended instead.

However, there are still a lot of older node packages that use left-pad as a dependency.
We don't want to not package them solely because left-pad is missing in Debian.

In order to overcome this dilemma I have created this repository. The intent is to maintain
this codebase with the help of the Debian JS Team. Which should be quite minimal as the code
is not expected to change.

# LICENSE

MIT
