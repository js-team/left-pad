'use strict'
module.exports = leftPad

function leftPad(str, len, ch) {
    return str.padStart(len, ch);
}
